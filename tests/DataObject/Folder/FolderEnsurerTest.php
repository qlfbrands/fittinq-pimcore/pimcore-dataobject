<?php declare(strict_types=1);

namespace Tests\Fittinq\Pimcore\DataObject\DataObject\Folder;

use Exception;
use Fittinq\Pimcore\DataObject\DataObject\Folder\FolderEnsurer;
use Fittinq\Pimcore\DataObject\DataObject\Folder\FolderRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FolderEnsurerTest extends KernelTestCase
{
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        static::bootKernel();
        static::$kernel->getContainer();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @throws Exception
     */
    public function test_folderShouldBeSavedToTheDatabaseWhenEnsured()
    {
        $folderRepository = new FolderRepository();
        $folder = $folderRepository->getFolder('/bicycle');
        $folder?->delete();

        $folderEnsurer = new FolderEnsurer();
        $folderEnsurer->createFolder('bicycle');

        $folderRepository = new FolderRepository();
        $actual = $folderRepository->getFolder('/bicycle');

        $this->assertEquals('/', $actual->getPath());
        $this->assertEquals('bicycle', $actual->getKey());
    }
}