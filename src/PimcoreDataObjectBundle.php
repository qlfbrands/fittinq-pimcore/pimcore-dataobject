<?php declare(strict_types=1);

namespace Fittinq\Pimcore\DataObject;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PimcoreDataObjectBundle extends Bundle
{
}
