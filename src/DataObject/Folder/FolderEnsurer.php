<?php declare(strict_types=1);

namespace Fittinq\Pimcore\DataObject\DataObject\Folder;

use Exception;
use Pimcore\Model\DataObject\Folder;
use Pimcore\Model\DataObject\Service;

class FolderEnsurer
{
    /**
     * @throws Exception
     */
    public function createFolder(string $path): Folder
    {
        return Service::createFolderByPath($path);
    }
}
